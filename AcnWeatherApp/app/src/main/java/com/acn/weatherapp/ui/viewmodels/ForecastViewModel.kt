package com.acn.weatherapp.ui.viewmodels

import androidx.lifecycle.ViewModel
import com.acn.weatherapp.dataproviders.ForecastDataProvides
import com.acn.weatherapp.models.WeatherDisplayItem
import com.acn.weatherapp.models.WeatherInfo
import com.acn.weatherapp.models.WeatherResp
import com.acn.weatherapp.utils.DataUtils
import com.acn.weatherapp.utils.ServiceCallback
import com.acn.weatherapp.utils.extensions.splitWithPosition
import com.acn.weatherapp.utils.extensions.toDate
import com.acn.weatherapp.utils.extensions.toFahrenheit
import com.acn.weatherapp.utils.extensions.toFormat


class ForecastViewModel : ViewModel() {

    val itemsToShow = arrayListOf<WeatherDisplayItem>()

    fun getForecastReport(
        location: String,
        callback: ServiceCallback<WeatherResp>
    ) {
        ForecastDataProvides().getWeatherReportsFromApi(location, callback)
    }

    fun prepareForecastItems(item: WeatherInfo?, country: String?): WeatherDisplayItem {

        return WeatherDisplayItem(
            item?.dtTxt?.toDate()?.toFormat()?.splitWithPosition(0),
            country,
            item?.dtTxt?.toDate()?.toFormat()?.splitWithPosition(2),
            item?.dtTxt?.toDate()?.toFormat()?.splitWithPosition(1),
            item?.main?.temp?.toFahrenheit(),
            item?.main?.tempMin?.toFahrenheit(),
            item?.main?.tempMax?.toFahrenheit(),
            "Humidity: ${item?.main?.humidity} %",
            "Wind:",
            "Pressure: ${item?.main?.pressure} hPa",
            "${item?.weather?.getOrNull(0)?.description?.toUpperCase()}",
            item?.weather?.getOrNull(0)?.icon,
            "${item?.clouds?.all} %"

        )
    }

    fun prepareDataForListAdapter(res: ArrayList<WeatherResp>): ArrayList<WeatherDisplayItem> {
        res?.map { resp ->
            val listItem = DataUtils.getTodayData(resp)
            val item = prepareForecastItems(listItem, resp.city?.name)
            itemsToShow.add(item)
        }
        return itemsToShow
    }

}