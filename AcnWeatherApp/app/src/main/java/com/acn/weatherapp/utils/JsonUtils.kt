package com.acn.weatherapp.utils

import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.JSON

object JsonUtils {
    fun responseParser(resClass:KSerializer<*>,result: String?):Any? {
        return  result?.let{
          JSON.parse(resClass, result)
        }
    }

}