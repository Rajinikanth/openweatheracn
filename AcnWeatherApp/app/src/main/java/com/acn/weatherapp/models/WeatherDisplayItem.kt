package com.acn.weatherapp.models

data class WeatherDisplayItem(
    val day:String?="",
    val country:String?="Singapore",
    val time: String? = "",
    val date: String? = "",
    val temperature: String? = "",
    val minTemperature: String? = "",
    val maxTemperature: String? = "",
    val humidity: String? = "",
    val windSpeed: String? = "",
    val pressure: String? = "",
    val weatherDesc: String? = "",
    val iconType: String? = "",
    val cloudPercentage: String? = ""
)