package com.acn.weatherapp.ui.holders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.acn.weatherapp.models.WeatherDisplayItem
import com.acn.weatherapp.utils.extensions.getDrawable
import kotlinx.android.synthetic.main.item_location_card.view.*

class WeatherItemViewHolder(view: View
) : RecyclerView.ViewHolder(view) {

    private var tvTempInfo = itemView.tvCurrentTemperatureInfo as TextView

    var carditem = itemView.cardItem as ConstraintLayout
    private var tvCountry = itemView.tvCountry as TextView

    private var tvPredictionText = itemView.tvPredictionText as TextView
    private var tvDayInfo = itemView.tvDayInfo as TextView
    private var tvDateInfo = itemView.tvDateInfo as TextView
    private var tvTimeInfo = itemView.tvTimeInfo as TextView
    private var tvHumidityInfo = itemView.tvHumidity as TextView
    private var imWeatherIcon = itemView.imageView as ImageView

    fun bindUIItems(items: WeatherDisplayItem?) {
        tvTempInfo?.text = items?.temperature

        imWeatherIcon.setImageResource(itemView.context.getDrawable("ic_${items?.iconType}"))
        tvCountry?.text = items?.country

        tvDayInfo.text = items?.day
        tvDateInfo?.text = items?.date
        tvTimeInfo?.text = items?.time

        tvHumidityInfo?.text = items?.humidity
        tvPredictionText?.text = items?.weatherDesc

    }
}