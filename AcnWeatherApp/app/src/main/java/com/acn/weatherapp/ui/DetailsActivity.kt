package com.acn.weatherapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.acn.weatherapp.R
import com.acn.weatherapp.models.WeatherDisplayItem
import com.acn.weatherapp.models.WeatherResp
import com.acn.weatherapp.ui.adapters.DetailsAdapter
import com.acn.weatherapp.ui.viewmodels.ForecastViewModel
import com.acn.weatherapp.utils.DataUtils
import com.acn.weatherapp.utils.ServiceCallback
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : ParentActivity() {

    var inputCountry = ""
    lateinit var detailsListAdapter: DetailsAdapter
     var mViewModel: ForecastViewModel?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        mViewModel = ViewModelProviders.of(this).get(ForecastViewModel::class.java)
        setActionBar()
        getExtras()
        setupItemListAdapter()
        getWeatherData()
    }

    private fun setupItemListAdapter() {
        detailsListAdapter = DetailsAdapter()
        listViewWeatherDetails?.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = detailsListAdapter
        }
    }

    private fun getWeatherData() {
        mViewModel?.getForecastReport(inputCountry, callback = object :
            ServiceCallback<WeatherResp> {
            override fun onSuccess(response: WeatherResp?) {
                val dataList = DataUtils.getFiveDaysData(response)
                val list = dataList?.map {
                    mViewModel?.prepareForecastItems(it, response?.city?.name) } as ArrayList<WeatherDisplayItem>
                detailsListAdapter.setItems(list)
            }
            override fun onError(error: Throwable) {
                //showScreenSnackMessage("oops,Something went wrong!!")
            }
        })

    }

    private fun getExtras() {
        inputCountry = intent.extras?.getString("Country") ?: "Singapore"
    }

}