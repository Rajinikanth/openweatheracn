package com.acn.weatherapp.utils

interface ServiceCallback<T> {
    fun onSuccess(response: T?)
    fun onError(error: Throwable)
}