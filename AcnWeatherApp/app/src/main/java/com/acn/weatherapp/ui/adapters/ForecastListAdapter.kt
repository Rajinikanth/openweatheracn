package com.acn.weatherapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.acn.weatherapp.R
import com.acn.weatherapp.ui.holders.WeatherItemViewHolder
import com.acn.weatherapp.models.WeatherDisplayItem

class ForecastListAdapter(callback: ListItemListener) : RecyclerView.Adapter<WeatherItemViewHolder>() {

    var listOfDisplayItems: ArrayList<WeatherDisplayItem>? = null

    interface ListItemListener {
        fun onItemClick(value:String?)
    }

    var listItemListener: ListItemListener?=null

    init {
        listItemListener = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_location_card,
            parent,
            false
        )
        return WeatherItemViewHolder(view)

    }

    override fun onBindViewHolder(holder: WeatherItemViewHolder, position: Int) {
        holder.bindUIItems(listOfDisplayItems?.getOrNull(position))
        holder.carditem.setOnClickListener {
            listItemListener?.onItemClick(listOfDisplayItems?.get(position)?.country)
        }
    }

    override fun getItemCount(): Int {
        return listOfDisplayItems?.size ?: 0
    }

    fun setItems(displayItems: ArrayList<WeatherDisplayItem>) {
        this.listOfDisplayItems = displayItems
        this.notifyDataSetChanged()
    }


}
