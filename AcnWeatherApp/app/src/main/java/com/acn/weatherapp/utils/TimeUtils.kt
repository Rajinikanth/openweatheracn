package com.acn.weatherapp.utils

import java.text.SimpleDateFormat
import java.util.*

object TimeUtils{

    fun compareDate(todayTime:String,reportTime:String):Boolean{
        val todayTimeArray = todayTime.split(":")
        val reportTimeArray = reportTime.split(":")
        return (todayTimeArray[0] < reportTimeArray[0])
    }

    fun getCurrentSystemTime():String{
        val date = Date()
        val strDateFormat = "hh:mm:ss"
        val dateFormat = SimpleDateFormat(strDateFormat)
        return dateFormat.format(date)
    }

    fun splitDateWithDayName(dateInfo: String?):List<String>? {
        return dateInfo?.split("-")
    }

}