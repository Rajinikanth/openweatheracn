package com.acn.weatherapp.ui

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.acn.weatherapp.R
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Share parent class will be use full to
 * handle common properties between the child activities
 */

open class ParentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        print("Child performing the view")
    }

    fun setActionBar() {
        supportActionBar?.title = this.getString(R.string.app_actionbar_title)
    }

    fun showScreenToastMessage(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()

    }

    fun showScreenSnackMessage(msg: String) {
        llRootView?.let {
            Snackbar.make(it, msg, Snackbar.LENGTH_SHORT).show()
        }
    }
}