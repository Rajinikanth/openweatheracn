package com.acn.weatherapp

import com.acn.weatherapp.api.BaseApi
import org.junit.Assert
import org.junit.Test

class ForecastingTestCases {
    @Test
    fun checkUrlAddQueryFormat() {

        val expectedUrlString = "http://api.openweathermap.org/data/2.5/forecast?q=Singapore&APPID=1234567"
        val forcast = "q"
        val appid = "APPID"

        val queryStringMap = hashMapOf<String, String>()
        queryStringMap[forcast] = "Singapore"
        queryStringMap[appid] = "1234567"

        try {
            val url = BaseApi().appendReqStringsToUrl("${BaseApi().BASE_API}forecast", queryStringMap)
            Assert.assertEquals(expectedUrlString, url)
        } catch (e: Exception) {
            throw AssertionError(e.message)
        }
    }
}