package com.acn.weatherapp.api

import com.acn.weatherapp.models.WeatherResp
import com.acn.weatherapp.utils.Constants
import com.acn.weatherapp.utils.ServiceCallback

class Forecast : BaseApi() {

    private val baseUrlWithAction = "$BASE_API$REQUEST_ACTION"

    fun getForecastDetails(
        reqLocation: String?,
        callback: ServiceCallback<WeatherResp>?
    ): String {
        var response:String?=null
        val queryStringMap = hashMapOf<String, String>()
        queryStringMap[QUERY_STRING_Q] = reqLocation ?: DEFAULTLOCATION
        queryStringMap[QUERY_STRING_APPID] = APPID
        try {
            response = serviceCall(appendReqStringsToUrl(baseUrlWithAction, queryStringMap)).toString()
        } catch (e: Exception) {
            callback?.onError(e)
        }
        return response?:Constants.DATA_UNDIFINED
    }
}