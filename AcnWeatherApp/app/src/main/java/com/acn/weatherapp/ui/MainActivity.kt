package com.acn.weatherapp.ui


import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.acn.weatherapp.R
import com.acn.weatherapp.models.WeatherResp
import com.acn.weatherapp.ui.adapters.ForecastListAdapter
import com.acn.weatherapp.ui.viewmodels.ForecastViewModel
import com.acn.weatherapp.utils.ServiceCallback
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : ParentActivity(), SearchView.OnQueryTextListener, ForecastListAdapter.ListItemListener {

    enum class Temparature{
        CELSIUS,FAHRENHEIT
    }
    
    lateinit var mViewModel: ForecastViewModel
    lateinit var weatherListAdapter: ForecastListAdapter
    lateinit var listListener: ForecastListAdapter.ListItemListener

    val locationList = arrayListOf("Singapore", "California")
    val locationResponseList = arrayListOf<WeatherResp>()

    var isErrrorOneTimeLoad = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setActionBar()
        setupItemListAdapter()
        svAddLocation?.setOnQueryTextListener(this)
        mViewModel = ViewModelProviders.of(this).get(ForecastViewModel::class.java)
        setUpConvertionControl()

    }

    private fun setUpConvertionControl(){
        temperatureChangeSwitch.text = this.getString(R.string.item_fahrenheit_code)

        temperatureChangeSwitch?.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                temperatureChangeSwitch.text = this.getString(R.string.item_celsius_code)
                updateTemperature(Temparature.CELSIUS)
            }else{
                temperatureChangeSwitch.text = this.getString(R.string.item_fahrenheit_code)
                updateTemperature(Temparature.FAHRENHEIT)
            }
        }
    }

    private fun updateTemperature(temp: Temparature) {
        when(temp){
            Temparature.FAHRENHEIT->{
                
            }
            else->{
                
            }
        }
    }

    override fun onResume() {
        super.onResume()
        getDefaultItemsData()
    }

    private fun setupItemListAdapter() {
        weatherListAdapter = ForecastListAdapter(this)
        listViewWeatherReport?.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = weatherListAdapter
        }
    }

    private fun getDefaultItemsData() {
        getLocationWeatherReport()
    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.itemAddLocation -> {
                if(svAddLocation?.visibility == View.VISIBLE){
                    svAddLocation?.visibility = View.GONE
                }else {
                    svAddLocation?.visibility = View.VISIBLE
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        println("Submit submit $query")
        isErrrorOneTimeLoad = false
        query?.let {
            svAddLocation?.visibility = View.GONE
            locationList.add(it)
            getLocationWeatherReport()
        } ?: run {
            showScreenToastMessage("please enter the location")
        }

        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return false
    }

    private fun getLocationWeatherReport() {

        locationResponseList.clear()
        mViewModel.itemsToShow.clear()

        locationList?.map {
            mViewModel.getForecastReport(it, callback = object :
                ServiceCallback<WeatherResp> {
                override fun onSuccess(response: WeatherResp?) {
                    response?.let {
                        locationResponseList.add(response)
                        if (locationResponseList.size == locationList.size) {
                            setResultToListAdapter()
                        }
                    } ?: run {
                        loadDataOnErrorCase(it)
                    }
                }

                override fun onError(error: Throwable) {
                    loadDataOnErrorCase(it)
                }
            })
        }
    }

    private fun loadDataOnErrorCase(removingCountry: String) {
        if (!isErrrorOneTimeLoad) {
            isErrrorOneTimeLoad = true
            locationList?.remove(removingCountry)
            showScreenSnackMessage("oops,Something went wrong!!")
            runOnUiThread {
                setResultToListAdapter()
            }
        }

    }

    private fun setResultToListAdapter() {
        val itemsToShpw = mViewModel.prepareDataForListAdapter(locationResponseList)
        weatherListAdapter.setItems(itemsToShpw)
    }



    override fun onItemClick(value: String?) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra("Country", value)
        startActivity(intent)

    }
}
