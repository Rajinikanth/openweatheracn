# OpenWeatherAcn

App Introduction

Weather update application will provide weather forecasting information based on the location, we can see all the forecasting information which will be provided by openweathermap Api.
There are two features which can help you to check the weather details
DashBoard : Where we can add the locations and see the current weather report
Details Report : Where we can see last five days information

Platform and Development IDE:
* Android  SDK
* Android Studio

Note: No 3 rd party libraries use in this development

Guidelines to use the code

Please check the master repo and clone in either cmd prompt or using by source tree standalone application
Go to Android studio and  from the file menu add existing project , Select the AcnWeatherApp folder
Create the Emulator using avd manager and run the application


Functionalities:

* Load Default location
* Add Location using Action menu
* Fetch Details from open weather
* Showing Card for selected locations
* Weather details



Code Modularities:

* UI - handles View , adapters and holders

* Data providers - Ui Talks to Providers for the data through View models

* Api layer - hables Payloads and threads to fetch data from api using android default client connection


Standard Code Structure:
	
	# UI
		Activities,Adapters,view models and holders
		
	# API
		Tasks,Payloads,data models
		
	# Provides
		FeatureSpecific Providers
	
	# utils
		Kotlin extenctions, constants,standalone files
		
		
		


More features can add:
- Converting Celsius to Fahrenheit
- Improve UI with latest UI components (Beautification)
- Charts and live notifications
- Storing historical data and sharing on social medium
- Integrating maps and rendering climate changes 
 - Alerts on emergency situations







