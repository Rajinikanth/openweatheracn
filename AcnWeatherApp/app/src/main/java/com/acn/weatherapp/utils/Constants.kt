package com.acn.weatherapp.utils

object Constants{

    val API_SUCCES_COD = "200"
    val INVALID_STATUS_CODES ="Invalid Status Codes"
    val RESP_ERROR_DATA_MISSING ="Data Missing"
    val RESP_ERROR_STATUS ="Error while consuming api , EX- If Appid not available we will get error"
    val DATA_UNDIFINED = "UNDEFINED"

}