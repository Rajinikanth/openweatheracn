package com.acn.weatherapp.api.tasks

import android.os.AsyncTask
import com.acn.weatherapp.api.Forecast
import com.acn.weatherapp.models.WeatherResp
import com.acn.weatherapp.utils.Constants
import com.acn.weatherapp.utils.JsonUtils
import com.acn.weatherapp.utils.ServiceCallback

class WeatherForecastTask : AsyncTask<String, Void, String>() {

    var callback: ServiceCallback<WeatherResp>? = null
    override fun doInBackground(vararg param: String?): String {
        return Forecast().getForecastDetails(param.getOrNull(0), callback)
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        try {
            val data = JsonUtils.responseParser(WeatherResp.serializer(), result) as? WeatherResp
            data?.let {
                when (data.cod?.contains(Constants.API_SUCCES_COD)) {
                    true -> callback?.onSuccess(data)
                    else -> callback?.onError(Throwable(message = Constants.INVALID_STATUS_CODES))
                }
            } ?: run {
                callback?.onError(Throwable(message = Constants.RESP_ERROR_DATA_MISSING))
            }

        } catch (e: Exception) {
            callback?.onError(Throwable(message = Constants.RESP_ERROR_STATUS))
        }
    }
}