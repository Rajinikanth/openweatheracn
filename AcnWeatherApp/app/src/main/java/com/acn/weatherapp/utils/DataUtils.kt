package com.acn.weatherapp.utils

import android.text.format.DateUtils
import com.acn.weatherapp.models.WeatherInfo
import com.acn.weatherapp.models.WeatherResp
import com.acn.weatherapp.ui.viewmodels.ForecastViewModel
import com.acn.weatherapp.utils.extensions.toDate
import java.util.*
import kotlin.collections.ArrayList

object DataUtils{

    fun getTodayData(response: WeatherResp?):WeatherInfo?{
        return response?.list?.filter {
            DateUtils.isToday(it?.dtTxt?.toDate()?.time ?: 0) }
            ?.sortedBy { it?.dtTxt }?.get(0)
    }

    fun getFiveDaysData(response: WeatherResp?):List<WeatherInfo?>?{
        val calendar = Calendar.getInstance()
       return response?.list?.groupBy { item ->
            calendar.time = item?.dtTxt?.toDate()
            calendar.get(Calendar.DAY_OF_MONTH)
        }?.map {
            it.value.sortedBy { it?.dtTxt }
        }?.map {
            it.getOrNull(0)
        }
    }

    fun getListDataFromTheResponse(
        res: ArrayList<WeatherResp>,
        mViewModel: ForecastViewModel
    ) {

    }
}