package com.acn.weatherapp.ui.holders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.acn.weatherapp.models.WeatherDisplayItem
import com.acn.weatherapp.utils.extensions.getDrawable
import kotlinx.android.synthetic.main.item_details_card.view.*


class ItemDetailsHolder(
    view: View
) : RecyclerView.ViewHolder(view) {


    private var imWeatherIcon = itemView.ivWeatherIcon as ImageView
    private var tvCelsius = itemView.tVTempInfo as TextView
    private var tvDayInfo = itemView.tvDayInfo as TextView
    private var tvDateInfo = itemView.tvDateInfo as TextView
    private var tvTimeInfo = itemView.tvTimeInfo as TextView

    private var tvCountry = itemView.tvCountryInfo as TextView
    //    private var tvPredictionText = itemView.tvPredictionText as TextView
    private var tvTodayDate = itemView.tvDayInfo as TextView
    private var tvWtrStatus = itemView.tvWeatherStatus as TextView

    fun bindData(items: WeatherDisplayItem?) {
        imWeatherIcon.setImageResource(itemView.context.getDrawable("ic_${items?.iconType}"))
        tvCelsius?.text = items?.temperature
        tvCountry?.text = items?.country
        tvTodayDate.text = items?.date
        tvWtrStatus?.text = items?.weatherDesc
        tvDayInfo?.text = items?.day
        tvDateInfo?.text= items?.date
        tvTimeInfo?.text= items?.time
//        tvPredictionText?.text = items?.weatherDesc
    }
}