package com.acn.weatherapp.api

import org.json.JSONObject
import java.net.URI
import java.net.URISyntaxException
import java.net.URL

open class BaseApi {


    val BASE_API = "http://api.openweathermap.org/data/2.5/"
    val APPID = "323c8cf1139f7b18b6232781e5637415"
    //323c8cf1139f7b18b6232781e5637415
    val QUERY_STRING_Q = "q"
    val QUERY_STRING_APPID = "APPID"
    val REQUEST_ACTION = "forecast"
    val DEFAULTLOCATION = "Singapore"


    /**
     * Perform service call
     * @param url
     * @return new String url with given parameters
     */
    fun serviceCall(url: String): JSONObject {
        val url = URL(url)
        println("URL :: $url")
        val urlConnection = url.openConnection()
        val lines = urlConnection.getInputStream().use { it.bufferedReader().readText() }
        println("lines :: $lines")

        return JSONObject(lines)
    }


    /**
     * Append parameters to given url
     * @param url
     * @param parameters
     * @return new String url with given parameters
     * @throws URISyntaxException
     */

    @Throws(URISyntaxException::class)
    fun appendReqStringsToUrl(url: String, parameters: HashMap<String, String>): String {
        val uri = URI(url)
        val builder = StringBuilder()
        for (entry in parameters.entries) {
            val keyValueParam = entry.key + "=" + entry.value
            if (builder.toString().isNotEmpty())
                builder.append("&")

            builder.append(keyValueParam)
        }
        val newUri = URI(uri.scheme, uri.authority, uri.path, builder.toString(), uri.fragment)
        return newUri.toString()
    }
}