package com.acn.weatherapp.utils.extensions

import android.content.Context
import java.text.SimpleDateFormat
import java.util.*

fun String.toFahrenheit(): String = "${"%.2f".format((this.toDouble() - 273.15) *  9/5 +32)} °F"
fun String.toCelsious(): String = "${"%.2f".format((this.toDouble() - 273.15) )} °C"
fun String.toDate(): Date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this)
fun Context.getDrawable(resId: String): Int = resources.getIdentifier(resId, "drawable", packageName)
fun Date.toFormat(format: String = "EEEE -dd MMM, yyyy -h:mma "): String = SimpleDateFormat(format).format(this)
fun String.splitWithPosition(pos:Int):String = this.split("-")[pos]