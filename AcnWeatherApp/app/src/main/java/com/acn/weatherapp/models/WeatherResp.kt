package com.acn.weatherapp.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


/**
 * Data Collected by Tool Provided by
 * android studio to convert json to kotlin data classes
 * (Plugin for Kotlin to convert Json String into Kotlin data class code quickly
 * Fast use it with short cut key ALT + K on Windows or Option + K on Mac )
 */

@Serializable
data class WeatherResp(
    val city: City? = null,
    val cnt: Int? = null,
    val cod: String? = null,
    val list: List<WeatherInfo?>? = null,
    val message: Double? = null
)


@Serializable
data class City(
    @SerialName("coord") val coord: Coord? = null,
    @SerialName("country") val country: String? = null,
    @SerialName("id") val id: Int? = null,
    @SerialName("name") val name: String? = null,
    @SerialName("population") val population: Int? = null,
    @SerialName("timezone") val timezone: Int? = null
)

@Serializable
data class Coord(
    @SerialName("lat") val lat: String? = null,
    @SerialName("lon") val lon: String? = null
)

@Serializable
data class WeatherInfo(
    @SerialName("clouds") val clouds: Clouds? = null,
    @SerialName("dt") val dt: Long? = null,
    @SerialName("dt_txt") val dtTxt: String? = null,
    @SerialName("main") val main: Main? = null,
    @SerialName("rain") val rain: Rain? = null,
    @SerialName("sys") val sys: Sys? = null,
    @SerialName("weather") val weather: List<Weather?>? = null,
    @SerialName("wind") val wind: Wind? = null
)

@Serializable
data class Weather(
    @SerialName("description") val description: String? = null,
    @SerialName("icon") val icon: String? = null,
    @SerialName("id") val id: Int? = null,
    @SerialName("main") val main: String? = null
)

@Serializable
data class Wind(
    @SerialName("deg") val deg: Double? = null,
    @SerialName("speed") val speed: Double? = null
)


@Serializable
data class Clouds(
    @SerialName("all")
    val all: Int? = null
)

@Serializable
data class Rain(
    @SerialName("3h") val h: Double? = null
)

@Serializable
data class Main(
    @SerialName("grnd_level") val grndLevel: String? = null,
    @SerialName("humidity") val humidity: String? = null,
    @SerialName("pressure") val pressure: String? = null,
    @SerialName("sea_level") val seaLevel: String? = null,
    @SerialName("temp") val temp: String? = null,
    @SerialName("temp_kf") val tempKf: String? = null,
    @SerialName("temp_max") val tempMax: String? = null,
    @SerialName("temp_min") val tempMin: String? = null
)

@Serializable
data class Sys(
    @SerialName("pod") val pod: String? = null
)
