package com.acn.weatherapp.ui.adapters

import com.acn.weatherapp.ui.holders.ItemDetailsHolder


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.acn.weatherapp.R
import com.acn.weatherapp.models.WeatherDisplayItem

class DetailsAdapter : RecyclerView.Adapter<ItemDetailsHolder>() {

    var listOfDisplayItems: ArrayList<WeatherDisplayItem>? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemDetailsHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_details_card,
            parent,
            false
        )
        return ItemDetailsHolder(view)

    }

    override fun onBindViewHolder(holder: ItemDetailsHolder, position: Int) {
        holder.bindData(listOfDisplayItems?.getOrNull(position))
    }



    override fun getItemCount(): Int {
        return listOfDisplayItems?.size ?: 0
    }

    fun setItems(displayItems: ArrayList<WeatherDisplayItem>) {
        this.listOfDisplayItems = displayItems
        this.notifyDataSetChanged()
    }
}
