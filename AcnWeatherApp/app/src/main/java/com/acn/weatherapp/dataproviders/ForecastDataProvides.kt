package com.acn.weatherapp.dataproviders

import com.acn.weatherapp.api.tasks.WeatherForecastTask
import com.acn.weatherapp.models.WeatherResp
import com.acn.weatherapp.utils.ServiceCallback


class ForecastDataProvides {
    fun getWeatherReportsFromApi(query: String, callback: ServiceCallback<WeatherResp>) {
        WeatherForecastTask().apply {
            this.callback = callback
        }.execute(query)
    }
}